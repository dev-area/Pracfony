FROM php:7.4-fpm

RUN apt-get -y update \
&& apt-get install -y libpq-dev libc-client-dev libkrb5-dev libicu-dev wget git unzip libxslt-dev libzip-dev libpng-dev libjpeg62-turbo-dev libfreetype6-dev libssl-dev libsodium-dev libcurl4-openssl-dev \
&& pecl install -o -f redis xdebug-2.9.4 \
&& docker-php-ext-enable redis \
&& docker-php-ext-enable xdebug \
&& docker-php-ext-configure intl \
&& docker-php-ext-configure gd --with-freetype --with-jpeg \
&& PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
&& docker-php-ext-install -j$(nproc) imap \
&& docker-php-ext-install intl pdo pdo_pgsql pgsql xsl zip sodium curl \
&& docker-php-ext-install -j$(nproc) gd




ADD docker/php/php.ini /usr/local/etc/php/php.ini

# Install Composer
RUN wget https://getcomposer.org/installer -O - -q \
    | php -- --install-dir=/bin --filename=composer --quiet

WORKDIR /var/www/pracfony